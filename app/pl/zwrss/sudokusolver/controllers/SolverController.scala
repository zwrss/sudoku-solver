package pl.zwrss.sudokusolver.controllers

import play.api._
import play.api.mvc._

object SolverController extends Controller {
  def index = Action {
    Ok(pl.zwrss.sudokusolver.views.html.index("Hello World!"))
  }
}
