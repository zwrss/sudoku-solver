package pl.zwrss.sudokusolver.model

import scala.collection.immutable.IndexedSeq

case class Board private(fields: Map[(Int, Int), Field]) {

  def removeOption(i: Int, j: Int, v: Int): Board = {
    val f: Field = fields(i,j)
    val newFields = fields.updated((i,j), f.removeOption(v))
    copy(fields = newFields)
  }

  def removeOptions(i: Int, j: Int, vs: Seq[Int]): Board = {
    val f: Field = fields(i,j)
    val newFields = fields.updated((i,j), f.removeOptions(vs))
    copy(fields = newFields)
  }

  def removeOptionsBox(b: Int, x: Int, vs: Seq[Int]): Board = {
    val (i, j) = resolveBoxCoords(b, x)
    val f: Field = fields(i,j)
    val newFields = fields.updated((i,j), f.removeOptions(vs))
    copy(fields = newFields)
  }

  def put(i: Int, j: Int, v: Int): Board = {
    val f: Field = fields(i,j)
    val newFields = fields.updated((i,j), f.put(v))
    copy(fields = newFields)
  }

  def getColumn(c: Int): Seq[Field] = (1 to 9).map(r => fields(r, c))

  def setColumn(c: Int, column: Seq[Field]): Board = {
    val newFields = (1 to 9).foldLeft(fields)((fields, r) => fields.updated((r, c), column(r-1)))
    copy(fields = newFields)
  }

  def getRow(r: Int): Seq[Field] = (1 to 9).map(c => fields(r, c))

  def setRow(r: Int, row: Seq[Field]): Board = {
    val newFields = (1 to 9).foldLeft(fields)((fields, c) => fields.updated((r, c), row(c-1)))
    copy(fields = newFields)
  }

  private def resolveBoxCorner(b: Int): (Int, Int) = b match {
    case 1 => 1 -> 1
    case 2 => 1 -> 4
    case 3 => 1 -> 7
    case 4 => 4 -> 1
    case 5 => 4 -> 4
    case 6 => 4 -> 7
    case 7 => 7 -> 1
    case 8 => 7 -> 4
    case 9 => 7 -> 7
  }

  private def resolveBoxInner(x: Int): (Int, Int) = (x-1) / 3 -> (x-1) % 3

  private def resolveBoxCoords(b: Int, x: Int): (Int, Int) = {
    val corner = resolveBoxCorner(b)
    val inner = resolveBoxInner(x)
    (corner._1 + inner._1) -> (corner._2 + inner._2)
  }

  def getBox(b: Int): Seq[Field] = (1 to 9).map(x => {
    val (r, c) = resolveBoxCoords(b, x)
    fields(r, c)
  })

  def setBox(b: Int, box: Seq[Field]): Board = {
    val newFields = (1 to 9).foldLeft(fields)((fields, x) => {
      val (r, c) = resolveBoxCoords(b, x)
      fields.updated((r, c), box(x-1))
    })
    copy(fields = newFields)
  }

  def solved: Boolean = fields.values.forall {
    case _: SolidValue => true
    case _ => false
  }

  override def toString: String = {
    (1 to 9).map(r => {
      getRow(r).map {
        case s: SolidValue => s.value
        case p: PencilValues => -p.values.head
      }.mkString(", ")
    }).mkString("\n")
  }
}

object Board {
  def blank: Board = {
    val keys: Seq[(Int, Int)] = (1 to 9).flatMap(i => (1 to 9).map(j => (i, j)))
    new Board(keys.map(k => k -> Field.blank).toMap)
  }
}

sealed trait Field {
  def removeOptions(os: Seq[Int]): Field
  def removeOption(o: Int): Field = removeOptions(Seq(o))
  def put(i: Int): Field = new SolidValue(i)
}

object Field {
  def blank: Field = new PencilValues(1 to 9)
}

case class SolidValue(value: Int) extends Field {
  override def removeOptions(os: Seq[Int]): Field = this
}

case class PencilValues(values: Seq[Int]) extends Field {
  override def removeOptions(os: Seq[Int]): Field = {
    val newVals = values.diff(os)
    if(newVals.size == 1) SolidValue(newVals.head)
    else PencilValues(newVals)
  }
}