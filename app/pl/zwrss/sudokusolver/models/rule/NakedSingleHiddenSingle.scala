package pl.zwrss.sudokusolver.model.rule

import pl.zwrss.sudokusolver.model._

object NakedSingleHiddenSingle extends Rule {
  override def apply(board: Board): Board = {

    def checkRow(board: Board, r: Int): Board = board.setRow(r, checkSeq(board.getRow(r)))

    def checkColumn(board: Board, c: Int): Board = board.setColumn(c, checkSeq(board.getColumn(c)))

    def checkBox(board: Board, b: Int): Board = board.setBox(b, checkSeq(board.getBox(b)))

    def checkSeq(seq: Seq[Field]): Seq[Field] = {
      val solids: Seq[Int] = seq.collect {
        case v: SolidValue => v.value
      }
      if(solids.nonEmpty) (1 to 9).foldLeft(Seq.empty[Field])((fields, x) => fields :+ seq(x-1).removeOptions(solids))
      else seq
    }

    (1 to 9).foldLeft(board)((board, i) => checkRow(checkColumn(checkBox(board, i), i), i))

  }
}
