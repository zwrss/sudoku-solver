package pl.zwrss.sudokusolver.model.rule

import pl.zwrss.sudokusolver.model.Board

trait Rule {
   def apply(board: Board): Board
 }
