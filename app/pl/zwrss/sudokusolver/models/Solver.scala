package pl.zwrss.sudokusolver.domain

import pl.zwrss.sudokusolver.model.Board
import pl.zwrss.sudokusolver.model.rule._

import scala.annotation.tailrec

object Solver {

  private val rules = Seq(
    NakedSingleHiddenSingle,
    LockedCandidate
  )

  @tailrec
  def solve(board: Board): Board = {
    val newBoard = rules.foldLeft(board)((board, rule) => rule.apply(board))
    if(board.solved || board == newBoard) newBoard
    else solve(newBoard)
  }

}
